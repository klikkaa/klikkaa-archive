# Currently using a max of 5 due to the size of the project
MAX_COUNT=5
COUNT=`grep --include=\*.rs --exclude-dir=target -rnw -e "FIXME" | wc -l`
echo "Found $COUNT FIXMEs"
if [ "$COUNT" -gt "$MAX_COUNT" ]; then
echo "Too many FIXMEs found in the code: max is $MAX_COUNT" >&2
exit 1
fi