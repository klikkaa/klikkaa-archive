# Currently using a max of 20 due to project size
MAX_COUNT=20
COUNT=`grep --include=\*.rs --exclude-dir=target -rnw -e "TODO" | wc -l`
echo "Found $COUNT TODOs"
if [ "$COUNT" -gt "$MAX_COUNT" ]; then
echo "Too many TODOs found in the code: max is $MAX_COUNT" >&2
exit 1
fi