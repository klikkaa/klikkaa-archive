# klikkaa-engine

The engine that klikkaa! is built upon. The code contained within this crate is licensed under the LGPLv3 license, unlike the main game.


There are examples of how to implement engine traits and functionality in third-party code within the engine. They are listed below.
* [Component](src/component/basic_renderable.rs)
