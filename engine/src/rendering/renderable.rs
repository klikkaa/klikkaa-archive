use super::ProjectionType;
use super::Vertex;
use png;
use std::io::Cursor;
use std::sync::Arc;
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer};
use vulkano::descriptor::descriptor_set::{DescriptorSet, PersistentDescriptorSet};
use vulkano::descriptor::pipeline_layout::PipelineLayoutAbstract;
use vulkano::device::{Device, Queue};
use vulkano::format::R8G8B8A8Srgb;
use vulkano::framebuffer::RenderPassAbstract;
use vulkano::image::{Dimensions, ImmutableImage};
use vulkano::pipeline::vertex::SingleBufferDefinition;
use vulkano::pipeline::GraphicsPipeline;
use vulkano::sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode};

use nalgebra_glm::{rotation, scale, translate, Mat4, Vec3};

/// Defines an interface for a renderable object
pub trait Renderable: Send + Sync {
    fn get_projection_type(&self) -> ProjectionType;
    /// Adds the commands needed to draw this renderable to the given command buffer
    fn get_render_info(&self) -> ();
}

/// An interface for all renderables that need to be initialized by a `Renderer` (or similar) component
pub trait RenderableRequiresInit<T: Renderable> {
    /// Allows the Renderable to properly initialize itself
    fn init(
        &self,
        pipeline: &Arc<
            GraphicsPipeline<
                SingleBufferDefinition<Vertex>,
                Box<dyn PipelineLayoutAbstract + Send + Sync>,
                Arc<dyn RenderPassAbstract + Send + Sync>,
            >,
        >,
        device: &Arc<Device>,
        queue: &Arc<Queue>,
        projection_buffer: std::sync::Arc<
            vulkano::buffer::cpu_pool::CpuBufferPoolSubbuffer<
                crate::component::vs::ty::Project,
                std::sync::Arc<vulkano::memory::pool::StdMemoryPool>,
            >,
        >,
        data_buffer_pool: vulkano::buffer::cpu_pool::CpuBufferPool<crate::component::vs::ty::Data>,
    ) -> T;
    fn get_projection_type(&self) -> ProjectionType;
}

/// A `RenderableSubcomponent` that needs to be initialized by a renderer
pub struct RenderableSubcomponentRequiresInit {
    position: Vec3,
    scale: Vec3,
    rotation: Vec3,
    image_location: String,
    projection_type: ProjectionType,
}

impl RenderableRequiresInit<RenderableSubcomponent> for RenderableSubcomponentRequiresInit {
    /// Allows the Renderable to properly initialize itself
    fn init(
        &self,
        pipeline: &Arc<
            GraphicsPipeline<
                SingleBufferDefinition<Vertex>,
                Box<dyn PipelineLayoutAbstract + Send + Sync>,
                Arc<dyn RenderPassAbstract + Send + Sync>,
            >,
        >,
        device: &Arc<Device>,
        queue: &Arc<Queue>,
        projection_buffer: std::sync::Arc<
            vulkano::buffer::cpu_pool::CpuBufferPoolSubbuffer<
                crate::component::vs::ty::Project,
                std::sync::Arc<vulkano::memory::pool::StdMemoryPool>,
            >,
        >,
        data_buffer_pool: vulkano::buffer::cpu_pool::CpuBufferPool<crate::component::vs::ty::Data>,
    ) -> RenderableSubcomponent {
        // TODO: Actually read a file

        // Create a texture with the given png file
        let (texture, _) = {
            // Read the file in from storage (or error)
            let png_bytes = include_bytes!("klikkaa_logo.png").to_vec();

            // Decode the PNG file
            let cursor = Cursor::new(png_bytes);
            let decoder = png::Decoder::new(cursor);
            let (info, mut reader) = decoder.read_info().unwrap();
            let dimensions = Dimensions::Dim2d {
                width: info.width,
                height: info.height,
            };
            let mut image_data = Vec::new();
            image_data.resize((info.width * info.height * 4) as usize, 0);
            reader.next_frame(&mut image_data).unwrap();

            // Create a renderable Image with it
            ImmutableImage::from_iter(
                image_data.iter().cloned(),
                dimensions,
                R8G8B8A8Srgb,
                queue.clone(),
            )
            .unwrap()
        };

        // Create the texture sampler for this image
        let sampler = Sampler::new(
            device.clone(),
            Filter::Linear,
            Filter::Linear,
            MipmapMode::Nearest,
            SamplerAddressMode::Repeat,
            SamplerAddressMode::Repeat,
            SamplerAddressMode::Repeat,
            0.0,
            1.0,
            0.0,
            0.0,
        )
        .unwrap();

        // Generate the matrices
        let mut TRS = scale(&Mat4::identity(), &self.scale);
        let rotation = {
            // Of course this has to be more complicated
            let x = rotation(
                self.rotation.x * std::f32::consts::PI,
                &Vec3::new(1.0, 0.0, 0.0),
            );
            let y = rotation(
                self.rotation.y * std::f32::consts::PI,
                &Vec3::new(0.0, 1.0, 0.0),
            );
            let z = rotation(
                self.rotation.z * std::f32::consts::PI,
                &Vec3::new(0.0, 0.0, 1.0),
            );

            // Multiply them together
            x * y * z
        };

        TRS *= rotation;
        TRS = translate(&TRS, &self.position);

        let TRS: [[f32; 4]; 4] = TRS.into();

        // Uniform buffer creation

        let uniform_buffer_subbuffer = {
            let data = crate::component::vs::ty::Data { TRS };

            data_buffer_pool.next(data).unwrap()
        };

        let layout = pipeline.layout().descriptor_set_layout(0).unwrap();

        let set = Arc::new(
            PersistentDescriptorSet::start(layout.clone())
                .add_sampled_image(texture.clone(), sampler.clone())
                .unwrap()
                .add_buffer(uniform_buffer_subbuffer.clone())
                .unwrap()
                .add_buffer(projection_buffer.clone())
                .unwrap()
                .build()
                .unwrap(),
        );

        // We use a hardcoded buffer since it'll get transformed later
        let vertex_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage::all(),
            false,
            [
                Vertex {
                    position: [-1.0, -1.0],
                },
                Vertex {
                    position: [-1.0, 1.0],
                },
                Vertex {
                    position: [1.0, 1.0],
                },
                Vertex {
                    position: [1.0, -1.0],
                },
            ]
            .iter()
            .cloned(),
        )
        .unwrap();

        RenderableSubcomponent {
            descriptor_set: set,
            TRS: TRS.into(),
            vertex_buffer,
        }
    }

    fn get_projection_type(&self) -> ProjectionType {
        self.projection_type
    }
}

/// A subcomponent which is renderable
///
/// Passed to a `Renderer` to render a component's graphical representation.
#[derive(Clone)]
pub struct RenderableSubcomponent {
    descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    // The position this renderable will be at (in window coordinates)
    TRS: Mat4,
    vertex_buffer: Arc<CpuAccessibleBuffer<[Vertex]>>,
}

impl RenderableSubcomponent {
    pub fn with_png_path(
        position: Vec3,
        rotation: Vec3,
        scale: Vec3,
        image_location: &str,
        projection_type: ProjectionType,
    ) -> RenderableSubcomponentRequiresInit {
        RenderableSubcomponentRequiresInit {
            image_location: image_location.to_owned(),
            position,
            rotation,
            scale,
            projection_type,
        }
    }
}

impl Renderable for RenderableSubcomponent {
    fn get_projection_type(&self) -> ProjectionType {
        unimplemented!()
    }
    /// Adds the commands needed to draw this renderable to the given command buffer
    fn get_render_info(&self) -> () {
        unimplemented!()
    }
}
