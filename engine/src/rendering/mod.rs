mod renderable;
pub use renderable::{Renderable, RenderableRequiresInit, RenderableSubcomponent};

/// Vertex type used by Vulkano for passing vertex buffers to shaders
#[derive(Default, Debug, Clone)]
pub struct Vertex {
    pub position: [f32; 2],
}
vulkano::impl_vertex!(Vertex, position);

/// The projection method that should be used to render an object
#[derive(Clone, Copy)]
pub enum ProjectionType {
    Perspective,
    Orthographic,
}
