use super::{Component, TickPriority};
use crate::engine::{Engine, EngineScene, SwapchainTuple};
use std::sync::{Arc, Mutex};
use vulkano::device::{Device, Queue};
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::swapchain::{
    AcquireError, Capabilities, ColorSpace, FullscreenExclusive, PresentMode, Surface,
    SurfaceTransform, Swapchain, SwapchainCreationError,
};
use winit::window::Window as WinitWindow;

/// Used to manage the window assigned to the current Scene
///
/// Window also handles input from the user.
pub struct Window {
    // Public members

    // Public (to the engine) members
    /// The surface that renders to the window
    pub(super) vulkan_surface: Arc<Surface<WinitWindow>>,
    /// The swapchain that is used to render to the window
    pub(super) vulkan_swapchain: SwapchainTuple,
    /// The size of the window (in pixels)
    pub(super) window_size: [u32; 2],
    // Private members
    parent: Option<Arc<Mutex<EngineScene>>>,
    component_name: String,
}

impl Window {
    /// Creates a new window
    ///
    /// This function is only intended to be called from the engine thread, to create a new window
    /// you must use the `Engine`'s function to do so.
    pub(crate) fn new(
        vulkan_window_surface: Arc<Surface<WinitWindow>>,
        engine_vulkan_instance: Arc<Instance>,
        engine_vulkan_device: Arc<Device>,
        engine_vulkan_physical_device: PhysicalDevice,
        engine_vulkan_queue: Arc<Queue>,
        size: [u32; 2],
    ) -> Box<Window> {
        // See if the window should be maximized on creation
        let maximized = {
            if size[0] == 0 && size[1] == 0 {
                // It should be maximized
                true
            } else {
                false
            }
        };

        // Get the surface capabilities
        let vulkan_surface_capabilities = vulkan_window_surface
            .capabilities(engine_vulkan_physical_device)
            .unwrap();

        // Create the swapchain for the window surface
        let (window_swapchain, window_dimensions) = Self::init_swapchain(
            vulkan_window_surface.clone(),
            engine_vulkan_device,
            engine_vulkan_queue,
            vulkan_surface_capabilities,
        );

        Box::new(Window {
            vulkan_surface: vulkan_window_surface,
            vulkan_swapchain: window_swapchain,
            window_size: window_dimensions,
            parent: None,
            component_name: "Engine_Window".to_owned(),
        })
    }

    /// Creates an initial swapchain for a window, returns the swapchain and the size of the window created
    fn init_swapchain(
        vulkan_surface: Arc<Surface<WinitWindow>>,
        engine_vulkan_device: Arc<Device>,
        engine_vulkan_queue: Arc<Queue>,
        vulkan_surface_capabilities: vulkano::swapchain::Capabilities,
    ) -> (SwapchainTuple, [u32; 2]) {
        // Get the dimensions of the window
        let window_dimensions = {
            let tuple: (u32, u32) = vulkan_surface
                .window()
                .inner_size()
                // Properly scale the inner size of the window
                .to_logical::<u32>(vulkan_surface.window().scale_factor())
                .into();
            // Convert the tuple into an array
            [tuple.0, tuple.1]
        };

        // Get the supported alpha mode for the window.
        let window_supported_alpha = vulkan_surface_capabilities
            .supported_composite_alpha
            .iter()
            // Get the first one
            .next()
            .unwrap();

        // Use the first supported format for images
        let window_image_format = vulkan_surface_capabilities.supported_formats[0].0;

        // Create the vulkan swapchain
        let swapchain = {
            Swapchain::new(
                engine_vulkan_device,
                vulkan_surface.clone(),
                vulkan_surface_capabilities.min_image_count,
                window_image_format,
                window_dimensions,
                1,
                vulkan_surface_capabilities.supported_usage_flags,
                &engine_vulkan_queue,
                SurfaceTransform::Identity,
                window_supported_alpha,
                PresentMode::Fifo,
                FullscreenExclusive::Default,
                true,
                ColorSpace::SrgbNonLinear,
            )
            .unwrap()
        };

        // Return the swapchain and dimensions
        (swapchain, window_dimensions)
    }

    pub fn get_vulkan_surface(&self) -> Arc<Surface<WinitWindow>> {
        self.vulkan_surface.clone()
    }

    pub fn get_vulkan_swapchain(&self) -> SwapchainTuple {
        self.vulkan_swapchain.clone()
    }

    pub fn get_size(&self) -> [u32; 2] {
        self.window_size
    }
}

impl Component for Window {
    fn tick(&self) {
        // Windows dont need to be ticked since they
    }
    fn fixed_tick(&self) {}
    fn get_name(&self) -> String {
        self.component_name.clone()
    }
    fn get_tick_priority(&self) -> TickPriority {
        TickPriority::Any
    }
    fn get_parent(&self) -> Option<Arc<Mutex<EngineScene>>> {
        if let Some(parent) = &self.parent {
            Some(parent.clone())
        } else {
            None
        }
    }

    fn on_awake(&self, _: Arc<Mutex<EngineScene>>) {}
}

pub struct InputStream {}
