use super::{Component, ComponentInitError, Positioned, Renderer, TickPriority};
use crate::engine::{EngineComponent, EngineScene};
use crate::rendering::{ProjectionType, RenderableSubcomponent};

use nalgebra_glm::{Quat, Vec3};
use std::sync::{Arc, Mutex};

/// A basic 2D renderable
///
/// This component is not meant for use in final code, it functions as an example on how
/// renderable components (and components in general) should be constructed.
/// It is, however, left in the engine for testing and example purposes.
pub struct BasicRenderable {
    // In the engine code, members exclusively used for traits are commonly prefixed
    // with the name of the trait, which is why it is done here.
    component_name: String,
    // Since multiple components (and the engine) need mutable access to the Scene
    // it is stored as a Mutex (inside an Arc). Try to keep it locked for as short
    // as possible.
    // The parent is an `Option` because it is possible to have an unparented
    // component. However, it will not tick as it is not assigned to a scene.
    component_parent: Option<Arc<Mutex<EngineScene>>>,
    // Note the difference between `Component` and `EngineComponent`, this is because
    // the engine stores components in a special way to allow them to be downcasted
    // back into their original concrete type. Like `component_parent`, this needs to
    // be stored as a Mutex.
    render_component: Arc<Mutex<Box<dyn EngineComponent + Send + Sync>>>,
    // This stores the component's position in the world
    component_position: Vec3,
    // This stores the component's rotation as a quaternion
    component_rotation: Quat,
    // This stores the data needed to render "this component", it is a "subcomponent"
    renderable_subcomponent: Option<RenderableSubcomponent>,
}

// Implement the code required for a renderer to render this object
impl Component for BasicRenderable {
    // The `tick` function is called after (before? doesn't matter) every frame
    // in things that get rendered, it is important to queue this object in its
    // renderer so it will get rendered in the next frame. The object is not
    // queued for the renderer automatically.
    fn tick(&self) {
        // There's nothing to do other than queue this component to the renderer
        // so we just add it to the renderer's queue.

        // Lock the mutex on the renderer. This is so we can submit to the renderer's
        // queue.
        let mutex_guard = &mut self.render_component.lock().unwrap();
        // Downcast the renderer from the way its stored. It is stored as a
        // `dyn EngineComponent` because there's no way to cast the mutex to a
        // renderer component.
        let render_component = mutex_guard.downcast_mut::<Renderer>().unwrap();

        // Ensure the renderable subcomponent was properly initialized, it should
        // have been when it became active, but it is best to check (and rust forces you to)
        if let Some(subcomponent) = &self.renderable_subcomponent {
            // It is properly initialized so it can be sent to the renderer,
            // submit our renderable subcomponent to the renderer. The subcomponent will
            // copy itself so there's no need to borrow or anything.
            render_component.enqueue_renderable(Box::new(subcomponent.clone()));
        } else {
            // // It wasn't properly initialized so we try again now, then submit it to the
            // // renderer.
            // let renderable_subcomponent = RenderableSubcomponent::with_png_path(
            //     Vec3::new(0.0, 0.0, 0.0),
            //     Vec3::new(0.0, 0.0, 0.0),
            //     Vec3::new(0.0, 0.0, 0.0),
            //     "fixme",
            // );

            // render_component.enqueue_renderable(Box::new(renderable_subcomponent.clone()));
            // self.renderable_subcomponent = Some(renderable_subcomponent);
        }

        // This isn't required here (since the block ends right away) but
        // it is recommended to drop locks as soon as possible to decrease locking
        // wait times (and therefore increase performance).
        drop(render_component);
    }

    // The `fixed_tick` function is fired at a fixed interval. This component
    // won't take advantage of the function so it is left empty.
    fn fixed_tick(&self) {
        // Framerate independent logic would go here, but since this is just a
        // renderable object there is nothing to be done.
    }

    // This function returns the name of the component, it is recommended to
    // prefix the default name of custom components with a namespace, but not
    // required or enforced in any way.
    fn get_name(&self) -> String {
        // Simply return the name that we have stored in the struct
        self.component_name.clone()
    }

    // This function defines the order that this component should be ticked.
    // Additionally, it can also specify if this component should be ticked at all.
    // Typically the priority is hardcoded.
    fn get_tick_priority(&self) -> TickPriority {
        // It doesn't matter when this object is ticked so we return the `Any` enumeration
        TickPriority::Any
    }

    // This function returns the parent of this component, which is always
    // going to be an `EngineScene`, or None.
    fn get_parent(&self) -> Option<Arc<Mutex<EngineScene>>> {
        // Return the parent that is stored by the component
        if let Some(parent) = &self.component_parent {
            Some(parent.clone())
        } else {
            None
        }
    }

    // This function is called on the component when it is added to a scene, since
    // that "wakes it up" because it will start ticking. This example doesn't need
    // the passed `EngineScene` so it is not used.
    fn on_awake(&self, _: Arc<Mutex<EngineScene>>) {
        // The `RenderableSubcomponent` that we are using requires some Renderer-specific
        // initialization in order to function, so that needs to be done with the target renderer.
        let renderable_subcomponent = Box::new(RenderableSubcomponent::with_png_path(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 0.0, 0.0),
            "fixme",
            ProjectionType::Orthographic,
        ));

        // The stored EngineComponent has previously been confirmed to be a `Renderer` so we do not
        // need to check again.
        let mut component_mutex = self.render_component.lock().unwrap();
        let renderer = component_mutex.downcast_mut::<Renderer>().unwrap();

        renderer.init_renderable(renderable_subcomponent);
    }
}

// Implement the interface to make this object have a position
impl Positioned for BasicRenderable {
    fn get_position(&self) -> Vec3 {
        // Simply return the current position of the component
        self.component_position
    }
    fn set_position(&mut self, position: Vec3) {
        // Assign the given position to the stored position
        self.component_position = position;
    }
    fn get_rotation(&self) -> Quat {
        // Return the current rotation (stored as a quaternion)
        self.component_rotation
    }
    fn set_rotation(&mut self, rotation: Quat) {
        // Assign the given rotation to the stored rotation
        self.component_rotation = rotation;
    }
}

impl BasicRenderable {
    // A function to create a new renderable
    pub fn new(
        name: Option<String>,
        position: Vec3,
        rotation: Quat,
        renderer: Arc<Mutex<Box<dyn EngineComponent + Send + Sync>>>,
    ) -> Result<Box<BasicRenderable>, ComponentInitError> {
        // Since the initialization of the rendering subcomponent is handled when the component
        // actually becomes active in the scene, there is no need to do anything other than validation
        // here.

        // Validate the the EngineComponent that was passed is a Renderer
        let locked_component = renderer.lock().unwrap();

        if !locked_component.is::<Renderer>() {
            // Return an error if the passed component was not the type we expected it to be
            return Err(ComponentInitError::IncorrectComponentType);
        };

        // Dropping the lock isn't that important but it is done just to prevent possible issues with wait times
        // on the mutex
        drop(locked_component);

        // Return a new box of Self
        Ok(Box::new(BasicRenderable {
            component_name: {
                if let Some(name) = name {
                    name
                } else {
                    "Engine_BasicRenderable".to_owned()
                }
            },
            component_parent: None,
            component_position: position,
            component_rotation: rotation,
            render_component: renderer,
            renderable_subcomponent: None,
        }))
    }
}
