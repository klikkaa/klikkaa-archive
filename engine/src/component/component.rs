use crate::engine::EngineScene;
use nalgebra_glm::{Quat, Vec3};
use std::sync::{Arc, Mutex};

/// The base trait for all components
/// 
/// Note that this is different than `EngineComponent`, that is for components stored in a scene
pub trait Component: Send + Sync {
    /// Fired before every frame
    fn tick(&self);
    /// Fired at fixed intervals
    fn fixed_tick(&self);
    /// Get the name of the Component
    /// Please namespace your component names to make debugging easier
    fn get_name(&self) -> String;
    fn get_tick_priority(&self) -> TickPriority;
    /// Get the parent scene of this component
    fn get_parent(&self) -> Option<Arc<Mutex<EngineScene>>>;
    /// Fired when a Component becomes active
    fn on_awake(&self, engine_scene: Arc<Mutex<EngineScene>>);
}

/// Used for `Component`s that have a position in the world
pub trait Positioned {
    fn get_position(&self) -> Vec3;
    fn set_position(&mut self, position: Vec3);
    fn get_rotation(&self) -> Quat;
    fn set_rotation(&mut self, rotation: Quat);
}

/// Defines when a `Component` should be ticked
pub enum TickPriority {
    /// Before any other components which are not also `First`
    First,
    /// Any: Any time before `Last` and after `First`
    Any,
    /// ast: After all `Any` and `First`
    Last,
    /// Never: Will never be ticked by the engine
    Never,
}
