mod basic_renderable;
mod camera;
mod component;
mod renderer;
mod window;

pub use basic_renderable::BasicRenderable;
pub use camera::Camera;
pub use component::{Component, Positioned, TickPriority};
pub use renderer::{fs, vs, Renderer};
pub use window::Window;

/// Error commonly returned by the `new` function of a component.
pub enum ComponentInitError {
    /// Returned when a passed EngineComponent cannot be downcasted to an expected type
    IncorrectComponentType,
}
