use super::{Component, Positioned, TickPriority};
use crate::engine::EngineScene;
use nalgebra_glm::{ortho, perspective, Mat4, Quat, Vec3};
use std::sync::{Arc, Mutex};

/// Manages a renderer's view of the world
///
/// Stores position and rotation data which can be used to generate projection matrices.
pub struct Camera {
    position: Vec3,
    rotation: Quat,
    component_parent: Option<Arc<Mutex<EngineScene>>>,
    component_name: String,
}

impl Camera {
    /// Returns a projection matrix based on the properties of this camera
    pub fn get_projection_matrix(target_dimensions: [u32; 2]) -> Mat4 {
        perspective(
            (target_dimensions[0] / target_dimensions[1]) as f32,
            85.0,
            0.0,
            1000.0,
        )
    }

    /// Returns an orthographic projection matrix based on the properties of this camera
    pub fn get_orthographic_matrix(target_dimensions: [u32; 2]) -> Mat4 {
        ortho(
            0.0,
            target_dimensions[0] as f32,
            0.0,
            target_dimensions[1] as f32,
            0.0,
            1000.0,
        )
    }
}

impl Component for Camera {
    fn tick(&self) {}
    /// Fired at fixed intervals
    fn fixed_tick(&self) {}

    fn get_name(&self) -> String {
        self.component_name.clone()
    }
    fn get_tick_priority(&self) -> TickPriority {
        TickPriority::Never
    }
    /// Get the parent scene of this component
    fn get_parent(&self) -> Option<Arc<Mutex<EngineScene>>> {
        if let Some(parent) = &self.component_parent {
            Some(parent.clone())
        } else {
            None
        }
    }

    fn on_awake(&self, _: Arc<Mutex<EngineScene>>) {}
}

impl Positioned for Camera {
    fn get_position(&self) -> Vec3 {
        self.position
    }
    fn set_position(&mut self, position: Vec3) {
        self.position = position;
    }
    fn get_rotation(&self) -> Quat {
        self.rotation
    }
    fn set_rotation(&mut self, rotation: Quat) {
        self.rotation = rotation;
    }
}
