use super::Window as WindowComponent;
use super::{Component, TickPriority};
use crate::engine::{Engine, EngineComponent, EngineScene};
use crate::rendering::ProjectionType;
use crate::rendering::{Renderable, RenderableRequiresInit, Vertex};
use nalgebra_glm::Mat4;
use std::sync::{Arc, Mutex};
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, CpuBufferPool, DeviceLocalBuffer};
use vulkano::command_buffer::DynamicState;
use vulkano::descriptor::pipeline_layout::PipelineLayoutAbstract;
use vulkano::device::{Device, Queue};
use vulkano::framebuffer::{Framebuffer, FramebufferAbstract, RenderPassAbstract, Subpass};
use vulkano::image::SwapchainImage;
use vulkano::pipeline::vertex::SingleBufferDefinition;
use vulkano::pipeline::viewport::Viewport;
use vulkano::pipeline::GraphicsPipeline;
use vulkano::sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode};
use vulkano::swapchain::Swapchain;
use winit::window::Window;

/// Used to render objects on screen
///
/// Renders objects to the scene's `Window` component.
pub struct Renderer {
    parent: Option<Arc<Mutex<EngineScene>>>,
    camera: Option<Arc<Mutex<Box<dyn EngineComponent + Send + Sync>>>>,
    vulkan_dynamic_state: DynamicState,
    vulkan_framebuffers: Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
    vulkan_device: Arc<Device>,
    vulkan_queue: Arc<Queue>,
    vulkan_graphics_pipeline: Arc<
        GraphicsPipeline<
            SingleBufferDefinition<Vertex>,
            Box<dyn PipelineLayoutAbstract + Send + Sync>,
            Arc<dyn RenderPassAbstract + Send + Sync>,
        >,
    >,
    vulkan_render_pass: Arc<dyn vulkano::framebuffer::RenderPassAbstract + Send + Sync>,
    vulkan_texture_sampler: Arc<Sampler>,
    data_buffer_pool: CpuBufferPool<super::vs::ty::Data>,
    proj_buffer_pool: CpuBufferPool<super::vs::ty::Project>,
    orthographic_projection_buffer: std::sync::Arc<
        vulkano::buffer::cpu_pool::CpuBufferPoolSubbuffer<
            super::vs::ty::Project,
            std::sync::Arc<vulkano::memory::pool::StdMemoryPool>,
        >,
    >,
    projection_projection_buffer: std::sync::Arc<
        vulkano::buffer::cpu_pool::CpuBufferPoolSubbuffer<
            super::vs::ty::Project,
            std::sync::Arc<vulkano::memory::pool::StdMemoryPool>,
        >,
    >,
    render_queue: Vec<Box<dyn Renderable>>,
    component_name: String,
}

impl Renderer {
    pub fn for_window(
        window: &WindowComponent,
        camera: Arc<Mutex<Box<dyn EngineComponent + Send + Sync>>>,
    ) -> (
        (Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>),
        Box<Renderer>,
    ) {
        // Panic if the window that the renderer is for has no parent
        if let None = window.get_parent() {
            panic!("Renderer::for_window should only be called with a Window that has a parent");
        }

        // Make sure the passed `camera` is actually a camera
        // TODO! :)

        // Get the engine instance
        let current_engine = Engine::get_engine();

        let vulkan_device = current_engine.get_graphics_device().unwrap();
        let vulkan_queue = current_engine.get_graphics_queue().unwrap();

        // Split the swapchain into its parts
        let (vulkan_swapchain, vulkan_swapchain_images) = window.get_vulkan_swapchain();

        // Create the render pass
        let vulkan_render_pass = Self::create_render_pass(&vulkan_swapchain, &vulkan_device);

        // Create the dynamic state
        let mut vulkan_dynamic_state = Self::create_dynamic_state();

        // Create the framebuffers used for rendering
        let vulkan_framebuffers = Self::create_framebuffers(
            &vulkan_device,
            &vulkan_swapchain_images.as_slice(),
            &vulkan_render_pass,
            &mut vulkan_dynamic_state,
        );

        let (vulkan_vertex_shader, vulkan_fragment_shader) = Self::init_shaders(&vulkan_device);

        let vulkan_graphics_pipeline = Self::create_graphics_pipeline(
            vulkan_vertex_shader,
            vulkan_fragment_shader,
            &vulkan_render_pass,
            &vulkan_device,
        );

        let vulkan_texture_sampler = Self::create_texture_sampler(&vulkan_device);

        // Reassemble the swapchain parts back into the complete version
        let vulkan_complete_swapchain = (vulkan_swapchain, vulkan_swapchain_images);

        // Create the buffer pools
        let data_buffer_pool =
            CpuBufferPool::<super::vs::ty::Data>::new(vulkan_device.clone(), BufferUsage::all());
        let proj_buffer_pool =
            CpuBufferPool::<super::vs::ty::Project>::new(vulkan_device.clone(), BufferUsage::all());

        // Create the projection buffers
        let empty_buffer: [[f32; 4]; 4] = Mat4::zeros().into();

        let orthographic_projection_buffer = {
            let data = super::renderer::vs::ty::Project { proj: empty_buffer };

            Arc::new(proj_buffer_pool.next(data).unwrap())
        };

        let projection_projection_buffer = {
            let data = super::renderer::vs::ty::Project { proj: empty_buffer };

            Arc::new(proj_buffer_pool.next(data).unwrap())
        };
        (
            vulkan_complete_swapchain,
            Box::new(Renderer {
                vulkan_dynamic_state,
                vulkan_framebuffers,
                vulkan_graphics_pipeline,
                vulkan_device,
                vulkan_queue,
                vulkan_render_pass,
                vulkan_texture_sampler,
                data_buffer_pool,
                proj_buffer_pool,
                orthographic_projection_buffer,
                projection_projection_buffer,
                render_queue: vec![],
                camera: Some(camera),
                parent: window.get_parent(),
                component_name: "Engine_Renderer".to_owned(),
            }),
        )
    }

    pub fn init_renderable<T: Renderable>(
        &self,
        renderable: Box<dyn RenderableRequiresInit<T>>,
    ) -> T {
        let projection_buffer = match renderable.get_projection_type() {
            ProjectionType::Orthographic => self.orthographic_projection_buffer.clone(),
            ProjectionType::Perspective => self.projection_projection_buffer.clone(),
        };

        renderable.init(
            &self.vulkan_graphics_pipeline,
            &self.vulkan_device,
            &self.vulkan_queue,
            projection_buffer,
            self.data_buffer_pool.clone(),
        )
    }

    pub fn enqueue_renderable(&mut self, renderable: Box<dyn Renderable>) {
        self.render_queue.push(renderable);
    }

    /// Creates a render pass for the renderer
    fn create_render_pass(
        vulkan_swapchain: &Arc<Swapchain<Window>>,
        vulkan_device: &Arc<Device>,
    ) -> Arc<dyn RenderPassAbstract + Send + Sync> {
        Arc::new(
            vulkano::single_pass_renderpass!(vulkan_device.clone(),
                attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: vulkan_swapchain.format(),
                    samples: 1,
                },
                depth: {
                    load: Clear,
                    store: DontCare,
                    format: vulkano::format::Format::D16Unorm,
                    samples: 1,
                }},
                pass: {
                    color: [color],
                    depth_stencil: {depth}
                }
            )
            .unwrap(),
        )
    }

    /// Creates a DynamicState that the renderer will use to render
    fn create_dynamic_state() -> DynamicState {
        DynamicState {
            line_width: None,
            viewports: None,
            scissors: None,
            compare_mask: None,
            write_mask: None,
            reference: None,
        }
    }

    /// Creates the needed framebuffers for rendering to surface
    fn create_framebuffers(
        device: &Arc<Device>,
        images: &[Arc<SwapchainImage<Window>>],
        render_pass: &Arc<dyn RenderPassAbstract + Send + Sync>,
        dynamic_state: &mut DynamicState,
    ) -> Vec<Arc<dyn FramebufferAbstract + Send + Sync>> {
        let dimensions = images[0].dimensions();

        let viewport = Viewport {
            origin: [0.0, 0.0],
            dimensions: [dimensions[0] as f32, dimensions[1] as f32],
            depth_range: 0.0..1.0,
        };

        dynamic_state.viewports = Some(vec![viewport]);

        // Create the depth buffer
        let depth_buffer = vulkano::image::AttachmentImage::transient(
            device.clone(),
            dimensions,
            vulkano::format::Format::D16Unorm,
        )
        .unwrap();

        images
            .iter()
            .map(|image| {
                Arc::new(
                    Framebuffer::start(render_pass.clone())
                        .add(image.clone())
                        .unwrap()
                        .add(depth_buffer.clone())
                        .unwrap()
                        .build()
                        .unwrap(),
                ) as Arc<dyn FramebufferAbstract + Send + Sync>
            })
            .collect::<Vec<_>>()
    }

    fn init_shaders(vulkan_device: &Arc<Device>) -> (vs::Shader, fs::Shader) {
        // TODO: Solution for the "hardcoded shaders" problem
        (
            vs::Shader::load(vulkan_device.clone()).unwrap(),
            fs::Shader::load(vulkan_device.clone()).unwrap(),
        )
    }

    fn create_graphics_pipeline(
        vertex_shader: vs::Shader,
        fragment_shader: fs::Shader,
        render_pass: &Arc<dyn RenderPassAbstract + Send + Sync>,
        device: &Arc<Device>,
    ) -> Arc<
        GraphicsPipeline<
            SingleBufferDefinition<Vertex>,
            Box<dyn PipelineLayoutAbstract + Send + Sync>,
            Arc<dyn RenderPassAbstract + Send + Sync>,
        >,
    > {
        Arc::new(
            GraphicsPipeline::start()
                // TODO: Going to want to have two buffers, one for vertices and one for normals
                .vertex_input_single_buffer()
                // Pass the entry point for the vertex shader
                .vertex_shader(vertex_shader.main_entry_point(), ())
                // How the vertex buffer is formatted (a list of triangles)
                .triangle_list()
                // Draw over the entire window
                .viewports_dynamic_scissors_irrelevant(1)
                // Pass the fragment shader
                .fragment_shader(fragment_shader.main_entry_point(), ())
                // The method to use to calculate depth
                .depth_stencil_simple_depth()
                // Indicate what render pass the pipeline will be used with
                .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
                .build(device.clone())
                // Panic if the build fails, there's no way to recover from this
                .unwrap(),
        )
    }

    fn create_texture_sampler(device: &Arc<Device>) -> Arc<Sampler> {
        Sampler::new(
            device.clone(),
            Filter::Linear,
            Filter::Linear,
            MipmapMode::Nearest,
            SamplerAddressMode::Repeat,
            SamplerAddressMode::Repeat,
            SamplerAddressMode::Repeat,
            0.0,
            1.0,
            0.0,
            0.0,
        )
        .unwrap()
    }
}

impl Component for Renderer {
    fn tick(&self) {
        panic!("Renderers should not tick with the engine. Doing so is not supported.");
    }

    fn fixed_tick(&self) {
        panic!("Renderers should not tick with the engine. Doing so is not supported.")
    }

    fn get_name(&self) -> String {
        self.component_name.clone()
    }

    fn get_tick_priority(&self) -> TickPriority {
        TickPriority::Never
    }

    fn get_parent(&self) -> Option<Arc<Mutex<EngineScene>>> {
        if let Some(parent) = &self.parent {
            Some(parent.clone())
        } else {
            None
        }
    }

    fn on_awake(&self, _: Arc<Mutex<EngineScene>>) {}
}

/// Temporary hardcoded vertex shader
pub mod vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        src: "
#version 450
layout(location = 0) in vec2 position;
layout(location = 0) out vec2 tex_coords;

layout(set = 0, binding = 1) uniform Data {
    mat4 TRS;
} model_uniforms;

layout(set = 0, binding = 2) uniform Project {
    mat4 proj;
} projection_uniforms;


void main() {
    gl_Position = vec4(position, 0.0, 1.0) * model_uniforms.TRS * projection_uniforms.proj;
    tex_coords = position + vec2(0.5);
}"
    }
}

/// Temporary hardcoded fragment shader
pub mod fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        src: "
#version 450
layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;
layout(set = 0, binding = 0) uniform sampler2D tex;
void main() {
    f_color = texture(tex, tex_coords);
}"
    }
}
