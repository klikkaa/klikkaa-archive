use super::EngineScene;
use crate::component::{Component, TickPriority};
use std::any::TypeId;
use std::sync::{Arc, Mutex};

/// Internal (Engine) component trait
///
/// This is used because `Any` cannot be used directly, so it wraps the functionality of an `Any` with additional
/// functionality required for component.
pub trait EngineComponent: 'static {
    fn type_id(&self) -> TypeId;
    fn tick(&self);
    fn fixed_tick(&self);
    fn get_name(&self) -> String;
    fn get_tick_priority(&self) -> TickPriority;
    fn on_awake(&self, engine_scene: Arc<Mutex<EngineScene>>);
}

impl<T: 'static + ?Sized + Component> EngineComponent for T {
    fn type_id(&self) -> TypeId {
        TypeId::of::<T>()
    }

    fn tick(&self) {
        self.tick();
    }

    fn fixed_tick(&self) {
        self.fixed_tick();
    }

    fn get_name(&self) -> String {
        self.get_name()
    }

    fn get_tick_priority(&self) -> TickPriority {
        self.get_tick_priority()
    }

    fn on_awake(&self, engine_scene: Arc<Mutex<EngineScene>>) {
        self.on_awake(engine_scene)
    }
}

impl dyn EngineComponent {
    pub fn is<T: EngineComponent>(&self) -> bool {
        // Get `TypeId` of the type this function is instantiated with.
        let t = TypeId::of::<T>();

        // Get `TypeId` of the type in the trait object.
        let concrete = self.type_id();

        // Compare both `TypeId`s on equality.
        t == concrete
    }

    pub fn downcast_ref<T: EngineComponent>(&self) -> Option<&T> {
        if self.is::<T>() {
            // SAFETY: just checked whether we are pointing to the correct type, and we can rely on
            // that check for memory safety because we have implemented Any for all types; no other
            // impls can exist as they would conflict with our impl.
            unsafe { Some(&*(self as *const dyn EngineComponent as *const T)) }
        } else {
            None
        }
    }

    pub fn downcast_mut<T: EngineComponent>(&mut self) -> Option<&mut T> {
        if self.is::<T>() {
            // SAFETY: just checked whether we are pointing to the correct type, and we can rely on
            // that check for memory safety because we have implemented Any for all types; no other
            // impls can exist as they would conflict with our impl.
            unsafe { Some(&mut *(self as *mut dyn EngineComponent as *mut T)) }
        } else {
            None
        }
    }
}

impl dyn EngineComponent + Send + Sync {
    pub fn is<T: EngineComponent>(&self) -> bool {
        EngineComponent::is::<T>(self)
    }

    pub fn downcast_ref<T: EngineComponent>(&self) -> Option<&T> {
        EngineComponent::downcast_ref::<T>(self)
    }

    pub fn downcast_mut<T: EngineComponent>(&mut self) -> Option<&mut T> {
        EngineComponent::downcast_mut::<T>(self)
    }
}
