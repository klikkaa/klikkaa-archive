mod engine;
mod engine_component;

pub use engine::{Engine, EngineScene};
pub use engine_component::EngineComponent;

use super::component::Window;

use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};
use vulkano::image::SwapchainImage;
use vulkano::swapchain::{Surface, Swapchain};
use winit::window::Window as WinitWindow;

/// An enum which wraps calls to the engine
pub enum EngineCall {
    // Forces the engine to shutdown, should have a reason passed
    ForceStop(String),
    // Tells the engine to open a window (and return the window component of the window)
    CreateWindow(Sender<EngineResponse>),
    // Asks very nicely for the vulkano Device
    GetGraphicsDevice(Sender<EngineResponse>),
    // Get the vulkano queue that is chosen by the engine
    GetGraphicsQueue(Sender<EngineResponse>),
    // Get the vulkano instance that the engine is using
    GetGraphicsInstance(Sender<EngineResponse>),
    // Register a scene on the engine
    RegisterEngineScene(Sender<EngineResponse>, EngineScene),
}

/// An enum returned by the Engine when calls are made to it
pub enum EngineResponse {
    // Response for EngineCall:InitNewWindow
    WindowCreation(Result<Window, ()>),
    // Response for a vulkano window creation
    VulkanoWindowCreation(Arc<Surface<WinitWindow>>),
    // Response for EngineCall::GetGraphicsDevice
    GraphicsDevice(Result<Arc<vulkano::device::Device>, ()>),
    // Response for EngineCall::GetGraphicsQueue
    GraphicsQueue(Result<Arc<vulkano::device::Queue>, ()>),
    // Response for EngineCall::GetGraphicsInstance
    GraphicsInstance(Result<Arc<vulkano::instance::Instance>, ()>),
    // Response for EngineCall::RegisterEngineScene
    EngineScene(Arc<Mutex<EngineScene>>),

    // Backend call responses
    // Response for BackendCall::GetEngineStatus
    EngineStatus(EngineStatus),
}

/// Calls to the engine involving internal engine state and window events
pub(super) enum BackendCall {
    // Get the Engine's status
    GetEngineStatus(std::sync::mpsc::Sender<EngineResponse>),
    // Send a window event to the engine to be processed
    SendWindowEvent(u32, winit::event::Event<'static, WinitWindow>),
    // Ask for a window to be created (with vulkano)
    CreateVulkanoWindow(Sender<EngineResponse>),
}

/// Calls to the main thread (for windowing)
pub(super) enum MainThreadCall {
    // Create a vulkano window
    CreateWindow(Sender<EngineResponse>),
}

/// Represents the current reported status of the engine
pub enum EngineStatus {
    OK,
    STOPPING,
}

/// Defines the projection matrix to use for rendering to a layer
pub enum ProjectionType {
    // Normal Projection Matrix
    Projection,
    // Normal Orthographic Matrix
    Orthographic,
}

/// A type to reduce complexity in struct signatures and return types
pub(crate) type SwapchainTuple = (
    Arc<Swapchain<WinitWindow>>,
    Vec<Arc<SwapchainImage<WinitWindow>>>,
);
