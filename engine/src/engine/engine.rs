use super::{BackendCall, EngineCall, EngineResponse, EngineStatus, MainThreadCall};
use crate::component::Window;
use std::collections::HashMap;
use std::mem::MaybeUninit;
use std::sync::mpsc::{channel, Receiver, SendError, Sender};
use std::sync::{Arc, Mutex, MutexGuard};
use std::thread;
use vulkano::device::{Device, Queue};
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano_win::VkSurfaceBuild;
use winit::event_loop::EventLoop;
use winit::window::WindowBuilder;

static mut CURRENT_INSTANCE: MaybeUninit<Engine> = MaybeUninit::<Engine>::uninit();

/// An object which represents the currently running engine
#[derive(Clone)]
pub struct Engine {
    call_handle: Sender<EngineCall>,
    event_loop_handle: Sender<BackendCall>,
}

impl Engine {
    /// Runs the Engine
    ///
    /// Must be called from the main thread (thread created at the entry point of the program)
    /// not doing so will result in errors.
    pub fn run<F>(&self, closure: F)
    // TODO (when non-experimental): Take a closure that never returns instead of panicking when it does
    where
        F: 'static + Fn(Engine) + Send,
    {
        let (tx, rx) = channel();

        // Start the engine, the caller will get nothing back from us
        Self::init_engine(rx, tx, closure);
    }

    pub fn send(&self, call: EngineCall) -> Result<(), SendError<EngineCall>> {
        self.call_handle.send(call)
    }

    /// Engine Init
    ///
    /// Engine Init (preinit, graphics init, windowing init) -> Engine Setup -> Provided Closure (-> winit event loop)
    fn init_engine<F>(receiver: Receiver<EngineCall>, sender: Sender<EngineCall>, closure: F)
    where
        F: 'static + Fn(Engine) + Send,
    {
        // Debug logging

        // Update the major major version letter when needed.
        // A = Alpha
        // C = Custom
        // R = Release
        // P = Pre-Release
        println!(
            "Running version (a_{version}) of klikkaa!",
            version = env!("CARGO_PKG_VERSION")
        );

        // Pre-init
        Self::engine_preinit();

        // Init Window Graphics
        let (graphics_instance, graphics_device, graphics_queue, _extra_queue) =
            Self::engine_graphics_init();

        // Create a winit event loop
        let engine_window_event_loop = EventLoop::new();

        // Create the channel for backend messaging
        let (backend_tx, backend_rx) = channel();

        // Create the channel for main thread messaging
        let (main_tx, main_rx) = channel();

        // We're done with init, prepare to create the child thread which will run the game
        // Create an `Engine` object which will be used to make calls to the engine
        let engine = Engine {
            call_handle: sender,
            event_loop_handle: backend_tx,
        };

        // We can now set the engine init status
        unsafe {
            CURRENT_INSTANCE.as_mut_ptr().write(engine.clone());
        }

        let engine_data = EngineState {
            graphics_instance,
            graphics_device,
            graphics_queue,
            windows: HashMap::new(),
            active_scenes: vec![],
        };

        // Start the engine's threads
        Self::engine_thread(receiver, backend_rx, main_tx, engine_data);

        let cloned_engine = engine.clone();

        // Spawn the thread with the passed closure
        thread::spawn(move || {
            closure(cloned_engine);
            // Panic if the closure returns on its own, it should never do that.
            panic!("Closure returned!");
        });

        // Start winit's event loop, we lose control of the main thread
        engine_window_event_loop.run(move |_event, event_loop, _control_flow| {
            // Move the main thread's rx into here
            let rx = &main_rx;

            // Check if there are any commands waiting for responses
            if let Ok(data) = rx.try_recv() {
                match data {
                    MainThreadCall::CreateWindow(tx) => {
                        let surface = WindowBuilder::new()
                            // Set the title (include the version as well)
                            // Remove the "ALPHA BUILD" part when we release the first version
                            .with_title(format!(
                                "ALPHA BUILD! klikkaa! {ver}",
                                ver = env!("CARGO_PKG_VERSION")
                            ))
                            // Maximize it upon creation
                            // TODO: Most likely going to want exclusive fullscren also configured here
                            .with_maximized(true)
                            // Build the surface (will also take ownership of the window)
                            .build_vk_surface(&event_loop, engine.get_graphics_instance().unwrap())
                            // Panic if window creation fails (since there's really no way to handle it well)
                            .unwrap();
                        tx.send(EngineResponse::VulkanoWindowCreation(surface))
                            .unwrap();
                    }
                }
            }
        });
    }

    /// Gets the current engine instance
    /// This will never fail for third-party code since it will panic if it fails
    pub fn get_engine() -> &'static Engine {
        unsafe { &*CURRENT_INSTANCE.as_ptr() }
    }

    /// Detects things about the system to make decisions on how to initialize the engine
    fn engine_preinit() {
        // TODO: Detect if this system is VR capable
    }

    /// Internal function which contains... the entire engine
    fn engine_thread(
        external_call_rx: Receiver<EngineCall>,
        backend_call_rx: Receiver<BackendCall>,
        main_thread_call_tx: Sender<MainThreadCall>,
        engine_data: EngineState,
    ) {
        // External call thread
        thread::spawn(move || {
            let mut engine_data = engine_data.clone();
            // loop... forever!
            loop {
                match external_call_rx.recv().unwrap() {
                    EngineCall::ForceStop(reason) => {
                        panic!("ForceStop sent! Reason: {}", reason);
                    }
                    EngineCall::CreateWindow(tx) => {}
                    EngineCall::GetGraphicsDevice(tx) => {
                        tx.send(EngineResponse::GraphicsDevice(Ok(engine_data
                            .graphics_device
                            .clone())))
                            .unwrap();
                    }
                    EngineCall::GetGraphicsQueue(tx) => {
                        tx.send(EngineResponse::GraphicsQueue(Ok(engine_data
                            .graphics_queue
                            .clone())))
                            .unwrap();
                    }
                    EngineCall::GetGraphicsInstance(tx) => {
                        tx.send(EngineResponse::GraphicsInstance(Ok(engine_data
                            .graphics_instance
                            .clone())))
                            .unwrap();
                    }
                    EngineCall::RegisterEngineScene(tx, scene) => {
                        // We own the scene now so we store it and give out the mutex for it
                        let owned_scene = Arc::new(Mutex::new(scene));
                        engine_data.active_scenes.push(owned_scene.clone());

                        // Send the scene back
                        tx.send(EngineResponse::EngineScene(owned_scene)).unwrap();
                    }
                }
            }
        });

        // Backend call thread
        thread::spawn(move || loop {
            match backend_call_rx.recv().unwrap() {
                BackendCall::GetEngineStatus(resp) => {
                    resp.send(EngineResponse::EngineStatus(EngineStatus::OK))
                        .unwrap();
                }
                BackendCall::SendWindowEvent(_, _) => {}
                BackendCall::CreateVulkanoWindow(tx) => {
                    main_thread_call_tx
                        .send(MainThreadCall::CreateWindow(tx))
                        .unwrap();
                }
            }
        });
    }

    /// Get the graphics device of the engine
    pub fn get_graphics_device(&self) -> Result<Arc<Device>, ()> {
        // A channel to get a response back with
        let (tx, rx) = channel();

        self.call_handle
            .send(EngineCall::GetGraphicsDevice(tx))
            .unwrap();

        if let EngineResponse::GraphicsDevice(resp) = rx.recv().unwrap() {
            // Return the response from the engine
            return resp;
        } else {
            panic!("Unexpected response type from engine call.");
        }
    }

    /// Get the device queue of the engine
    pub fn get_graphics_queue(&self) -> Result<Arc<Queue>, ()> {
        // A channel to get a response back with
        let (tx, rx) = channel();

        self.call_handle
            .send(EngineCall::GetGraphicsQueue(tx))
            .unwrap();

        if let EngineResponse::GraphicsQueue(resp) = rx.recv().unwrap() {
            // Return the response from the engine
            return resp;
        } else {
            panic!("Unexpected response type from engine call.");
        }
    }

    /// Internal function to init graphics structures
    fn engine_graphics_init() -> (Arc<Instance>, Arc<Device>, Arc<Queue>, Arc<Queue>) {
        // Create a vulkan instance
        let instance = {
            // Let vulkan know we want to create an instance that has all the capabilities required for rendering
            // to a window. These are non-core so we have to explicitly ask for them nicely.
            let extensions = vulkano_win::required_extensions();

            // Create the instance
            // Panic with helpful text :)
            Instance::new(None, &extensions, None)
                .expect("[ENGINE]: Failed to create vulkan instance.")
        };

        // TODO:
        // Allow the user to pick a device to use (for users with more than one graphics-capable device)
        // Add additional checks for supported-ness

        // (Currently) Get the first graphics-capable device in the system
        let physical = PhysicalDevice::enumerate(&instance)
            .next()
            // Error if we cant find any devices that support vulkan
            .expect("[ENGINE]: No vulkan-capable device available.");
        // Log which device is being used
        println!(
            "Using device: {} (type: {:?})",
            physical.name(),
            physical.ty()
        );

        // Get a graphics queue family from the device we are using
        // This queue supports basically every operation and should be okay for the time being
        let queue_family = physical
            .queue_families()
            // Take the first queue that supports graphics
            .find(|&q| q.supports_graphics())
            .expect("[ENGINE]: Failed to find a queue family.");

        // Instantiate a vulkan device as well as get its queues (that use the target queue family from above)
        let (device, mut queues) = {
            // Define the extensions the engine requires of the device
            let device_ext = vulkano::device::DeviceExtensions {
                // Required for rendering to a window
                khr_swapchain: true,
                ..vulkano::device::DeviceExtensions::none()
            };

            // Create the device
            Device::new(
                physical,
                physical.supported_features(),
                &device_ext,
                [(queue_family, 0.5)].iter().cloned(),
            )
            .expect("[ENGINE]: Failed to create vulkan device.")
        };

        // Grab the first two graphics-capable queues
        // We grab two so that we can have an extra one for special engine features
        let window_queue = queues.next().unwrap();
        let extra_queue = queues.next().unwrap();

        // Return all the structures renderers will need
        (instance, device, window_queue, extra_queue)
    }

    /// Get the vulkano instance the engine is using
    pub fn get_graphics_instance(&self) -> Result<Arc<Instance>, ()> {
        // A channel to get a response back with
        let (tx, rx) = channel();

        self.call_handle
            .send(EngineCall::GetGraphicsInstance(tx))
            .unwrap();

        if let EngineResponse::GraphicsInstance(resp) = rx.recv().unwrap() {
            // Return the response from the engine
            return resp;
        } else {
            panic!("Unexpected response type from engine call.");
        }
    }

    /// Returns the physical device that is being used. Probably a hack.
    fn get_physical_device(instance: &Arc<Instance>) -> PhysicalDevice<'_> {
        PhysicalDevice::enumerate(instance).next().unwrap()
    }

    pub(super) fn add_scene(&self, scene: EngineScene) -> Arc<Mutex<EngineScene>> {
        // A channel to get a response back with
        let (tx, rx) = channel();

        self.call_handle
            .send(EngineCall::RegisterEngineScene(tx, scene))
            .unwrap();

        if let EngineResponse::EngineScene(resp) = rx.recv().unwrap() {
            // Return the response from the engine
            return resp;
        } else {
            panic!("Unexpected response type from engine call.");
        }
    }

    fn create_window(self) -> Box<Window> {
        // A channel to get a response from the backend with
        let (tx, rx) = channel();

        self.event_loop_handle
            .send(BackendCall::CreateVulkanoWindow(tx))
            .unwrap();

        let window_surface;
        if let EngineResponse::VulkanoWindowCreation(resp) = rx.recv().unwrap() {
            // Return the response from the engine
            window_surface = resp;
        } else {
            panic!("Unexpected response type from engine call.");
        }

        Window::new(
            window_surface,
            self.get_graphics_instance().unwrap(),
            self.get_graphics_device().unwrap(),
            Self::get_physical_device(&self.get_graphics_instance().unwrap()),
            self.get_graphics_queue().unwrap(),
            [1000, 1000],
        )
    }
}

#[derive(Clone)]
struct EngineState {
    pub(super) graphics_device: Arc<Device>,
    pub(super) graphics_queue: Arc<Queue>,
    pub(super) graphics_instance: Arc<Instance>,
    pub(super) windows: HashMap<u32, Sender<winit::event::Event<'static, winit::window::Window>>>,
    pub(super) active_scenes: Vec<Arc<Mutex<EngineScene>>>,
}

use super::engine_component::EngineComponent;

/// A scene which holds components and manages ticking logic for them
pub struct EngineScene {
    pub(super) children: Vec<Arc<Mutex<Box<dyn EngineComponent + Sync + Send>>>>,
}

impl EngineScene {
    /// Create a new scene with nothing in it.
    /// The `EngineScene` object MUST be registered with the engine (which will then take control over it)
    pub fn new() -> Arc<Mutex<EngineScene>> {
        Arc::new(Mutex::new(EngineScene { children: vec![] }))
    }

    pub fn add(&mut self, component: Box<dyn EngineComponent + Send + Sync>) {
        self.children.push(Arc::new(Mutex::new(component)));
    }

    /// Gets the first child of the given type
    pub fn get_first_child<T: EngineComponent>(
        &self,
    ) -> Option<MutexGuard<Box<dyn EngineComponent + Send + Sync>>> {
        for child_arc in &self.children {
            // Lock the mutex (owned by the arc)
            let child = child_arc.lock().unwrap();

            // Check if it is the type we are looking for
            if child.is::<T>() {
                return Some(child);
            }
        }

        None
    }

    /// Get all children of the given type
    pub fn get_all_children<T: EngineComponent>(
        &self,
    ) -> Option<Vec<MutexGuard<Box<dyn EngineComponent + Send + Sync>>>> {
        let mut results = vec![];
        for child_arc in &self.children {
            // Lock the mutex (owned by the arc)
            let child = child_arc.lock().unwrap();

            // Check if it is the type we are looking for
            if child.is::<T>() {
                results.push(child);
            }
        }

        // Return None if we got no results
        if results.is_empty() {
            None
        } else {
            Some(results)
        }
    }

    /// Instruction given by some source to `tick`
    /// note that `tick` is different than `fixed_tick`, tick runs based on framerate, `fixed_tick` runs at fixed intervals
    pub fn tick_scene(&self) {}
}
